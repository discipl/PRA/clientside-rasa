from rasa.nlu.classifiers.keyword_intent_classifier import KeywordIntentClassifier
from rasa.nlu.extractors.entity_synonyms import EntitySynonymMapper
from rasa.nlu.extractors.regex_entity_extractor import RegexEntityExtractor
from rasa.nlu.featurizers.dense_featurizer.spacy_featurizer import SpacyFeaturizer
from rasa.nlu.featurizers.sparse_featurizer.count_vectors_featurizer import (
    CountVectorsFeaturizer,
)
from rasa.nlu.featurizers.sparse_featurizer.regex_featurizer import RegexFeaturizer
from rasa.nlu.tokenizers.whitespace_tokenizer import WhitespaceTokenizer
from rasa.core.policies.rule_policy import RulePolicy

DEFAULT_COMPONENTS = [
    # Message Classifiers
    KeywordIntentClassifier,
    # Message Entity Extractors
    EntitySynonymMapper,
    RegexEntityExtractor,
    # Message Feauturizers
    SpacyFeaturizer,
    CountVectorsFeaturizer,
    RegexFeaturizer,
    # Tokenizers
    WhitespaceTokenizer,
    # Dialogue Management Policies
    RulePolicy,
]