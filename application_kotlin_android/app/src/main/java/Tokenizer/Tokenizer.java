package Tokenizer;

import java.util.List;

// Interface for tokenizing text
public interface Tokenizer {
    // Method to tokenize a given text
    List<String> tokenize(String text);
}
