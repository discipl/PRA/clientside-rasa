package Tokenizer;

import java.util.ArrayList;
import java.util.List;

// BasicTokenizer class implements the Tokenizer interface
public class BasicTokenizer implements Tokenizer {
    // Default to lowercasing tokens
    private boolean do_lower_case = true;
    // List of tokens that should never be split
    private final List<String> never_split = new ArrayList<>();
    // Option to tokenize Chinese characters
    private boolean tokenize_chinese_chars = true;

    // Constructor with parameters for customization
    public BasicTokenizer(boolean do_lower_case, List<String> never_split, boolean tokenize_chinese_chars) {
        this.do_lower_case = do_lower_case;
        // Ensure that never_split is initialized, even if provided as null
        if (never_split == null) {
            never_split = new ArrayList<>();
        }
        this.tokenize_chinese_chars = tokenize_chinese_chars;
    }

    // Default constructor
    public BasicTokenizer() {
    }

    // Implementation of the tokenize method from the Tokenizer interface
    @Override
    public List<String> tokenize(String text) {
        // Clean the input text using TokenizerUtils
        text = TokenizerUtils.cleanText(text);

        // Tokenize Chinese characters if specified
        if (tokenize_chinese_chars) {
            text = TokenizerUtils.tokenizeChineseChars(text);
        }

        // Tokenize the text into original tokens based on whitespace
        List<String> orig_tokens = TokenizerUtils.whitespaceTokenize(text);

        // Create a list to store the split tokens
        List<String> split_tokens = new ArrayList<>();

        // Process each original token
        for (String token : orig_tokens) {
            // Lowercase the token and handle accent stripping if specified
            if (do_lower_case && !never_split.contains(token)) {
                token = TokenizerUtils.runStripAccents(token.toLowerCase());
                // Split the token on punctuation and add to the list
                split_tokens.addAll(TokenizerUtils.runSplitOnPunctuation(token, never_split));
            }
        }

        // Tokenize the split tokens based on whitespace and return the result
        return TokenizerUtils.whitespaceTokenize(String.join(" ", split_tokens));
    }
}
