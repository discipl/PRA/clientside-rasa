package Tokenizer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// Tokenizer implementing the WordPiece algorithm
public class WordpieceTokenizer implements Tokenizer {
    // Vocabulary mapping tokens to their IDs
    private final Map<String, Integer> vocab;
    // Token representing unknown words
    private final String unk_token;
    // Maximum number of characters per word
    private final int max_input_chars_per_word;

    // Constructor with specified maximum characters per word
    public WordpieceTokenizer(Map<String, Integer> vocab, String unk_token, int max_input_chars_per_word) {
        this.vocab = vocab;
        this.unk_token = unk_token;
        this.max_input_chars_per_word = max_input_chars_per_word;
    }

    // Default constructor with a maximum of 100 characters per word
    public WordpieceTokenizer(Map<String, Integer> vocab, String unk_token) {
        this.vocab = vocab;
        this.unk_token = unk_token;
        this.max_input_chars_per_word = 100;
    }

    @Override
    public List<String> tokenize(String text) {
        // List to store the output tokens
        List<String> output_tokens = new ArrayList<>();

        // Tokenize the input text
        for (String token : TokenizerUtils.whitespaceTokenize(text)) {
            // Check if the token exceeds the maximum characters allowed
            if (token.length() > max_input_chars_per_word) {
                output_tokens.add(unk_token);
                continue;
            }

            // Variables for tracking wordpiece tokenization
            boolean is_bad = false;
            int start = 0;

            // List to store sub-tokens
            List<String> sub_tokens = new ArrayList<>();

            // Perform WordPiece tokenization
            while (start < token.length()) {
                int end = token.length();
                String cur_substr = "";

                // Iterate to find the longest matching substring in the vocabulary
                while (start < end) {
                    String substr = token.substring(start, end);
                    if (start > 0) {
                        substr = "##" + substr;
                    }
                    if (vocab.containsKey(substr)) {
                        cur_substr = substr;
                        break;
                    }
                    end -= 1;
                }

                // If no valid substring is found, mark as bad
                if (cur_substr.equals("")) {
                    is_bad = true;
                    break;
                }

                // Add the current substring to the sub-tokens list
                sub_tokens.add(cur_substr);
                start = end;
            }

            // Add either the unknown token or the sub-tokens to the output
            if (is_bad) {
                output_tokens.add(unk_token);
            } else {
                output_tokens.addAll(sub_tokens);
            }
        }

        // Return the final list of output tokens
        return output_tokens;
    }
}
