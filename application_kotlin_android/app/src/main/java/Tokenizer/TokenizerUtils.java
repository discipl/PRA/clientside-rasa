package Tokenizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Utility class for tokenization-related functions
public class TokenizerUtils {

    // Performs invalid character removal and whitespace cleanup on text
    public static String cleanText(String text) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            Character c = text.charAt(i);
            int cp = (int) c;
            if (cp == 0 || cp == 0xFFFD || _is_control(c)) {
                continue;
            }
            if (_is_whitespace(c)) {
                output.append(" ");
            } else {
                output.append(c);
            }
        }
        return output.toString();
    }

    // Adds whitespace around any CJK character
    public static String tokenizeChineseChars(String text) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            Character c = text.charAt(i);
            int cp = (int) c;
            if (_is_chinese_char(cp)) {
                output.append(" ");
                output.append(c);
                output.append(" ");
            } else {
                output.append(c);
            }
        }
        return output.toString();
    }

    // Runs basic whitespace cleaning and splitting on a piece of text
    public static List<String> whitespaceTokenize(String text) {
        text = text.trim();
        if (!text.equals("")) {
            return Arrays.asList(text.split("\\s+"));
        }
        return new ArrayList<>();
    }

    // Strips accents from a token
    public static String runStripAccents(String token) {
        token = Normalizer.normalize(token, Form.NFD);
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < token.length(); i++) {
            Character c = token.charAt(i);
            if (Character.NON_SPACING_MARK != Character.getType(c)) {
                output.append(c);
            }
        }
        return output.toString();
    }

    // Splits punctuation on a piece of text
    public static List<String> runSplitOnPunctuation(String token, List<String> never_split) {
        List<String> output = new ArrayList<>();
        if (never_split != null && never_split.contains(token)) {
            output.add(token);
            return output;
        }

        boolean start_new_word = true;
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < token.length(); i++) {
            Character c = token.charAt(i);
            if (_is_punctuation(c)) {
                if (str.length() > 0) {
                    output.add(str.toString());
                    str.setLength(0);
                }
                output.add(c.toString());
                start_new_word = true;
            } else {
                if (start_new_word && str.length() > 0) {
                    output.add(str.toString());
                    str.setLength(0);
                }
                start_new_word = false;
                str.append(c);
            }
        }
        if (str.length() > 0) {
            output.add(str.toString());
        }
        return output;
    }

    // Generates a token ID map from a vocabulary file
    public static Map<String, Integer> generateTokenIdMap(InputStream file) throws IOException {
        HashMap<String, Integer> token_id_map = new HashMap<>();
        if (file == null)
            return token_id_map;

        try (BufferedReader br = new BufferedReader(new InputStreamReader(file))) {
            String line;
            int index = 0;
            while ((line = br.readLine()) != null) {
                token_id_map.put(line, index);
                index += 1;
            }
        }
        return token_id_map;
    }

    // Checks whether `chars` is a punctuation character
    private static boolean _is_punctuation(char c) {
        if (((int) c >= 33 && (int) c <= 47) || ((int) c >= 58 && (int) c <= 64) || ((int) c >= 91 && (int) c <= 96)
                || ((int) c >= 123 && (int) c <= 126)) {
            return true;
        }
        int charType = Character.getType(c);
        return Character.CONNECTOR_PUNCTUATION == charType || Character.DASH_PUNCTUATION == charType
                || Character.END_PUNCTUATION == charType || Character.FINAL_QUOTE_PUNCTUATION == charType
                || Character.INITIAL_QUOTE_PUNCTUATION == charType || Character.OTHER_PUNCTUATION == charType
                || Character.START_PUNCTUATION == charType;
    }

    // Checks whether `chars` is a whitespace character
    private static boolean _is_whitespace(char c) {
        if (c == ' ' || c == '\t' || c == '\n' || c == '\r') {
            return true;
        }
        int charType = Character.getType(c);
        return Character.SPACE_SEPARATOR == charType;
    }

    // Checks whether `chars` is a control character
    private static boolean _is_control(char c) {
        if (c == '\t' || c == '\n' || c == '\r') {
            return false;
        }
        int charType = Character.getType(c);
        return Character.CONTROL == charType || Character.DIRECTIONALITY_COMMON_NUMBER_SEPARATOR == charType
                || Character.FORMAT == charType || Character.PRIVATE_USE == charType || Character.SURROGATE == charType
                || Character.UNASSIGNED == charType;
    }

    // Checks whether CP is the codepoint of a CJK character
    private static boolean _is_chinese_char(int cp) {
        return (cp >= 0x4E00 && cp <= 0x9FFF) || (cp >= 0x3400 && cp <= 0x4DBF) || (cp >= 0x20000 && cp <= 0x2A6DF)
                || (cp >= 0x2A700 && cp <= 0x2B73F) || (cp >= 0x2B740 && cp <= 0x2B81F)
                || (cp >= 0x2B820 && cp <= 0x2CEAF) || (cp >= 0xF900 && cp <= 0xFAFF)
                || (cp >= 0x2F800 && cp <= 0x2FA1F);
    }
}
