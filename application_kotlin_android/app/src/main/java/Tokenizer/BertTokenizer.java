package Tokenizer;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Constructs a BERT tokenizer. Based on WordPiece.
 * <p>
 * This tokenizer inherits from :class:`~transformers.PreTrainedTokenizer` which
 * contains most of the methods. Users should refer to the superclass for more
 * information regarding methods.
 * <p>
 * Args:
 * <p>
 * vocab_file (:obj:`string`): File containing the vocabulary.
 * ...
 */
public class BertTokenizer implements Tokenizer {

    // Default vocabulary file path
    private String vocab_file = "res/raw/vocab_for_distilbert_en_nl.txt";
    private Map<String, Integer> token_id_map;
    private Map<Integer, String> id_token_map;
    private boolean do_lower_case = false;
    private boolean do_basic_tokenize = false;
    private List<String> never_split = new ArrayList<>();
    private String unk_token = "[UNK]";
    private String sep_token = "[SEP]";
    private String pad_token = "[PAD]";
    private String cls_token = "[CLS]";
    private String mask_token = "[MASK]";
    private boolean tokenize_chinese_chars = true;
    private BasicTokenizer basic_tokenizer;
    private WordpieceTokenizer wordpiece_tokenizer;

    private static final int MAX_LEN = 512;

    // Constructor with parameters for customization
    public BertTokenizer(String vocab_file, boolean do_lower_case, boolean do_basic_tokenize, List<String> never_split,
                         String unk_token, String sep_token, String pad_token, String cls_token, String mask_token,
                         boolean tokenize_chinese_chars) {
        this.vocab_file = vocab_file;
        this.do_lower_case = false;
        this.do_basic_tokenize = false;
        this.never_split = never_split;
        this.unk_token = unk_token;
        this.sep_token = sep_token;
        this.pad_token = pad_token;
        this.cls_token = cls_token;
        this.mask_token = mask_token;
        this.tokenize_chinese_chars = tokenize_chinese_chars;
        init();
    }

    // Default constructor, initializes the tokenizer
    public BertTokenizer() {
        init();
    }

    // Initialize method to load vocabulary and set up tokenizers
    private void init() {
        try {
            this.token_id_map = load_vocab(vocab_file);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        this.id_token_map = new HashMap<>();
        for (String key : token_id_map.keySet()) {
            this.id_token_map.put(token_id_map.get(key), key);
        }

        if (do_basic_tokenize) {
            this.basic_tokenizer = new BasicTokenizer(do_lower_case, never_split, tokenize_chinese_chars);
        }
        this.wordpiece_tokenizer = new WordpieceTokenizer(token_id_map, unk_token);
    }

    // Load vocabulary from file and return token ID map
    private Map<String, Integer> load_vocab(String vocab_file_name) throws IOException {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        assert classloader != null;
        InputStream file = classloader.getResourceAsStream(vocab_file_name);
        System.out.println(file);
        return TokenizerUtils.generateTokenIdMap(file);
    }

    /**
     * Tokenizes a piece of text into its word pieces.
     * <p>
     * This uses a greedy longest-match-first algorithm to perform tokenization
     * using the given vocabulary.
     * <p>
     * For example: input = "unaffable" output = ["un", "##aff", "##able"]
     * <p>
     * Args: text: A single token or whitespace separated tokens. This should have
     * already been passed through `BasicTokenizer`.
     * <p>
     * Returns: A list of wordpiece tokens.
     */
    @Override
    public List<String> tokenize(String text) {
        List<String> splitTokens = new ArrayList<>();
        if (do_basic_tokenize) {
            for (String token : basic_tokenizer.tokenize(text)) {
                splitTokens.addAll(wordpiece_tokenizer.tokenize(token));
            }
        } else {
            splitTokens = wordpiece_tokenizer.tokenize(text);
        }
        return splitTokens;
    }

    // Converts a sequence of tokens to a single string
    public String convertTokensToString(List<String> tokens) {
        return tokens.stream().map(s -> s.replace("##", "")).collect(Collectors.joining(" "));
    }

    // Converts a sequence of tokens to their corresponding IDs
    public List<Integer> convertTokensToIds(List<String> tokens) {
        List<Integer> output = new ArrayList<>();
        for (String s : tokens) {
            output.add(token_id_map.get(s));
        }
        return output;
    }

    // Returns the vocabulary size
    public int vocabSize() {
        return token_id_map.size();
    }

    // Converts a sequence of IDs to tokens
    public List<String> convertIdsToTokens(List<?> ids) {
        List<String> tokens = new ArrayList<>();
        for (Object idObj : ids) {
            if (idObj instanceof Integer) {
                Integer id = (Integer) idObj;
                if (id_token_map.containsKey(id)) {
                    tokens.add(id_token_map.get(id));
                } else {
                    tokens.add(unk_token);
                }
            } else if (idObj instanceof String) {
                String idString = (String) idObj;
                try {
                    Integer id = Integer.parseInt(idString);
                    if (id_token_map.containsKey(id)) {
                        tokens.add(id_token_map.get(id));
                    } else {
                        tokens.add(unk_token);
                    }
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException("Invalid ID format: " + idString);
                }
            } else {
                throw new IllegalArgumentException("Unsupported ID type: " + idObj.getClass());
            }
        }
        return tokens;
    }

    // Maps tokens to word indices, considering special tokens
    public List<Integer> mapTokensToWords(List<String> tokens) {
        List<Integer> tokenToWordIndices = new ArrayList<>();

        int wordIndex = 0;
        boolean resetIndex = false;

        for (String token : tokens) {
            if (isSpecialToken(token)) {
                if (token.equals(sep_token)) {
                    // Reset the index to 0 after encountering the first [SEP]
                    resetIndex = true;
                }
                tokenToWordIndices.add(null);
            } else {
                if (resetIndex) {
                    wordIndex = 0;
                    resetIndex = false;
                }
                tokenToWordIndices.add(wordIndex);
                // Increment word index for the next token
                wordIndex += getTokenWordCount(token);
            }
        }

        return tokenToWordIndices;
    }

    // Check if a token is a special token
    private boolean isSpecialToken(String token) {
        return token.equals(unk_token) || token.equals(sep_token) || token.equals(pad_token)
                || token.equals(cls_token) || token.equals(mask_token);
    }

    // Get the word count for a token
    private int getTokenWordCount(String token) {
        // Example: If token starts with "##", it's a sub-word, so return 0 (no new words)
        // Otherwise, treat it as a complete word, so return 1
        return token.startsWith("##") ? 0 : 1;
    }

    // Get original text indices from tokenToWordIndices within a specified range
    public List<Integer> getOriginalTextIndices(List<Integer> tokenToWordIndices, int startIndex, int endIndex) {
        List<Integer> originalTextIndices = new ArrayList<>();

        for (int i = startIndex; i <= endIndex; i++) {
            Integer wordIndex = tokenToWordIndices.get(i);
            if (wordIndex != null) {
                originalTextIndices.add(wordIndex);
            }
        }

        return originalTextIndices;
    }
}
