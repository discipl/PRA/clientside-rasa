package com.example.testapplicationbert

import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import ai.onnxruntime.*
import ai.onnxruntime.extensions.OrtxPackage
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.BackgroundColorSpan
import android.widget.ScrollView
import kotlin.math.roundToInt

// FullTextActivity.kt: Displays the full text and handles user interactions
class FullTextActivity : AppCompatActivity() {
    private var curEP: String = ""
    private lateinit var ortSession: OrtSession
    private var ortEnv: OrtEnvironment = OrtEnvironment.getEnvironment()
    private val messageEditText: EditText by lazy { findViewById(R.id.messageEditText) }
    private val sendButton: Button by lazy { findViewById(R.id.sendButton) }
    private val answerTextView: TextView by lazy { findViewById(R.id.answerTextView) }
    private val fullTextTextView: TextView by lazy { findViewById(R.id.fullTextTextView) }
    private val titleTextView: TextView by lazy { findViewById(R.id.titleTextView) }
    private val scrollView: ScrollView by lazy { findViewById(R.id.scrollView) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_text)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Retrieve title and fullText from the intent
        val title = intent.getStringExtra("title")
        val fullText = intent.getStringExtra("fullText")

        // Populate UI with title and fullText
        titleTextView.text = "$title"
        fullTextTextView.text = "$fullText"
        answerTextView.text = getString(R.string.here_the_answer_will_appear_magically)

        // Initialize Ort Session with NNAPI as the execution provider
        createOrtSession("NNAPI")

        // Set click listener for the send button to perform Q&A
        sendButton.setOnClickListener {
            try {
                // Perform Question and Answer
                performQA(ortSession)
            } catch (e: Exception) {
                // Handle exceptions
                e.printStackTrace()
                Toast.makeText(
                    baseContext,
                    "Failed to perform QuestionAnswering" + e.printStackTrace(),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    // Handle back button press
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()  // Go back to the previous activity
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    // Create an Ort Session with the specified execution provider
    private fun createOrtSession(ep: String) {
        if (curEP == ep) {
            return
        }
        curEP = ep
        // Initialize Ort Session with specified execution provider and options
        val sessionOptions: OrtSession.SessionOptions = OrtSession.SessionOptions()
        if (ep.contains("NNAPI")) {
            sessionOptions.addNnapi()
        } else if (ep.contains("XNNAPCK")) {
            val po = mapOf<String, String>()
            sessionOptions.addXnnpack(po)
        } else if (ep.contains("CPU")) {
            sessionOptions.addCPU(true)
        }
        sessionOptions.setSessionLogLevel(OrtLoggingLevel.ORT_LOGGING_LEVEL_VERBOSE)
        sessionOptions.setSessionLogVerbosityLevel(0)
        sessionOptions.registerCustomOpLibrary(OrtxPackage.getLibraryPath())
        ortSession = ortEnv.createSession(readModel(), sessionOptions)
    }

    // Clean up resources on activity destruction
    override fun onDestroy() {
        super.onDestroy()
        ortEnv.close()
        ortSession.close()
    }

    // Update UI elements with the Q&A result
    private fun updateUI(result: Result) {
        val defaultAns = "No answer found."
        val answer = if (result.outputAnswer == "[CLS]") defaultAns else result.outputAnswer

        // Highlight the answer in the full text
        val fullText = fullTextTextView.text.toString()
        val highlightedText = highlightAnswer(fullText, result.startIndex, result.endIndex)

        fullTextTextView.text = highlightedText
        answerTextView.text = answer
    }

    // Highlight the answer in the full text
    private fun highlightAnswer(
        fullText: String,
        startIndex: Int,
        endIndex: Int,
    ): SpannableString {
        // Check if the indexes indicate no answer found
        if (startIndex == 0 && endIndex == 0) {
            // Return a non-highlighted SpannableString
            return SpannableString(fullText)
        }

        // Split the full text into a list of words
        val wordList = fullText.split("(?<=\\p{Punct})|(?=\\p{Punct})|\\s+\n".toRegex())
            .flatMap { it.trim().split("\\s+".toRegex()) }

        // Ensure startIndex and endIndex are within bounds
        val actualStartIndex = startIndex.coerceIn(0, wordList.size - 1)
        val actualEndIndex = endIndex.coerceIn(0, wordList.size - 1)

        // Create a SpannableString from the wordList
        val spannableString = SpannableString(wordList.joinToString(" "))

        // Calculate the start and end indices within the joined string
        val start = wordList.subList(0, actualStartIndex).sumBy { it.length + 1 }
        val end = start + wordList.subList(actualStartIndex, actualEndIndex + 1)
            .sumBy { it.length + 1 } - 1

        // Define the span to highlight the specified range of words with a transparent yellow background
        val highlightColor =
            Color.argb(100, 255, 255, 0) // Adjust the alpha value (100) for transparency
        val highlightSpan = BackgroundColorSpan(highlightColor)

        // Apply the span to the range of words
        spannableString.setSpan(
            highlightSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        // Set the highlighted text to the TextView
        fullTextTextView.text = spannableString

        // Scroll to the first occurrence of the highlighted text
        val layout = fullTextTextView.layout
        val scrollY = layout.getLineTop(layout.getLineForOffset(start))
        scrollView.scrollTo(0, scrollY)

        return spannableString
    }

    // Read the ONNX model from the resources
    private fun readModel(): ByteArray {
        val modelID =
            R.raw.distilbert_base_en_nl_cased_finetuned_squad_quantized_with_pre_post_processing
        return resources.openRawResource(modelID).readBytes()
    }

    // Read the user's question from the EditText
    private fun readQuestion(): CharSequence {
        val userText = messageEditText.text
        return userText.ifEmpty { messageEditText.hint }
    }

    // Perform Question and Answer using the ONNX model and update the UI
    private fun performQA(ortSession: OrtSession) {
        val qaPerformer = QAPerformer()
        val result = qaPerformer.answer(fullTextTextView.text, readQuestion(), ortEnv, ortSession)
        updateUI(result)
    }
}
