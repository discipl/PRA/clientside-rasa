package com.example.testapplicationbert

import org.yaml.snakeyaml.Yaml

// YAML parser for extracting text items from a YAML file
class YamlParser() {
    // List to store parsed text items
    private var textItems: List<TextItem>? = null

    // Parse YAML file and return a list of TextItem objects
    fun parseYamlFile(fileName: String): List<TextItem> {
        // Create a YAML parser
        val yaml = Yaml()

        // Load YAML data from the input stream
        val inputStream = this::class.java.classLoader?.getResourceAsStream(fileName)
            ?: throw IllegalArgumentException("File not found: $fileName")
        val data = yaml.load(inputStream) as List<Map<String, String>>

        // Map YAML data to TextItem objects
        textItems = data.map { TextItem(it["title"]!!, it["fullText"]!!) }

        // Return the list of parsed text items
        return textItems!!
    }
}
