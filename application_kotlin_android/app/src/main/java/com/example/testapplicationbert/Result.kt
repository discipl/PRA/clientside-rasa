package com.example.testapplicationbert

// Data class to hold the result of the QA operation
data class Result(
    var outputAnswer: String = "",
    var startIndex: Int = 0,
    var endIndex: Int = 0,
)