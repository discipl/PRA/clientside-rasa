package com.example.testapplicationbert

import Tokenizer.BertTokenizer
import com.example.testapplicationbert.Result
import ai.onnxruntime.OnnxTensor
import ai.onnxruntime.OrtEnvironment
import ai.onnxruntime.OrtSession
import android.util.Log
import java.util.Collections


// Class responsible for performing Question-Answering using ONNX model
internal class QAPerformer {

    // Function to answer a question based on the provided article and ONNX session
    fun answer(
        articleSeq: CharSequence,
        questionSeq: CharSequence,
        ortEnv: OrtEnvironment,
        ortSession: OrtSession
    ): Result {
        val result = Result()

        // Step 1: Get article and question as string
        val article = articleSeq.toString()
        val question = questionSeq.toString()

        // Sample text for tokenization
        val text = "My name is Teodor and I live in Eindhoven"
        val tokenizer = BertTokenizer()

        // Tokenization and debugging logs
        val tokens = tokenizer.tokenize(text)
        val tokenIds = tokenizer.convertTokensToIds(tokens)
        Log.i("DEBUG_TOKENS", tokens.toString())
        Log.i("DEBUG_IDS", tokenIds.toString())
        Log.i("DEBUG_VOCAB_SIZE", tokenizer.vocabSize().toString())
        val textFromTokens = tokenizer.convertTokensToString(tokens)
        Log.i("DEBUG_TEXT_FROM_TOKENS", textFromTokens)
        val textFromIds = tokenizer.convertIdsToTokens(tokenIds)
        Log.i("DEBUG_TOKENS_FROM_IDS", textFromIds.toString())

        // Step 2: create shape [batch, sentence_num] and input Tensor
        val shape = longArrayOf(1, 2)
        val inputTensor = OnnxTensor.createTensor(ortEnv, arrayOf(question, article), shape)

        inputTensor.use {
            // Step 3: call ort inferenceSession run
            val output = ortSession.run(Collections.singletonMap("input_text", inputTensor))

            output.use {
                // Step 4: output analysis

                // Binary Mask for Tokenized text
                // in console 0 corresponds to text that is for the question
                // 1 for text that is for the context
                val rawOutput0 = (output?.get(0)?.value) as Array<LongArray>
                // Actual output as string
                val rawOutput1 = (output.get(1)?.value) as Array<String>
                // Start and end index respectively of above answer IN TOKENIZED TEXT
                val rawOutput2 = (output.get(2)?.value) as LongArray
                val rawOutput3 = (output.get(3)?.value) as LongArray
                // Tokenized Text
                val rawOutput4 = (output.get(4)?.value) as Array<LongArray>
                val resultList: List<String> =
                    rawOutput4.flatMap { it.map { it.toString() } }.toList()

                // BEGIN: Console outputs
                Log.i("DEBUG_BINARY_MASK", "Binary Mask for Tokenized text:")
                rawOutput0.forEach { row ->
                    Log.i("DEBUG_BINARY_MASK_ROW", row.joinToString(" "))
                }

                Log.i("DEBUG_ACTUAL_OUTPUT", "Actual output as string:")
                rawOutput1.forEach { row ->
                    Log.i("DEBUG_ACTUAL_OUTPUT_ROW", row)
                }

                Log.i(
                    "DEBUG_START_INDEX",
                    "Start index in tokenized text: ${rawOutput2.joinToString()}"
                )
                Log.i(
                    "DEBUG_END_INDEX",
                    "End index in tokenized text: ${rawOutput3.joinToString()}"
                )

                Log.i("DEBUG_TOKENIZED_TEXT", "Tokenized Text:")
                rawOutput4.forEach { row ->
                    Log.i("DEBUG_TOKENIZED_TEXT_ROW", row.joinToString(" "))
                }

                Log.i("DEBUG_INFERENCE_IDS", "Inference Result IDs: $resultList")
                val tokensFromResult = tokenizer.convertIdsToTokens(resultList)
                Log.i("DEBUG_INFERENCE_TOKENS", "Inference Result Tokens: $tokensFromResult")
                Log.i(
                    "DEBUG_INFERENCE_MAPPING",
                    "Mapping Tokens to Words: ${tokenizer.mapTokensToWords(tokensFromResult)}"
                )

                // END: Console Outputs

                // Step 5: set output result
                result.outputAnswer = rawOutput1[0]
                val textIndices = tokenizer.getOriginalTextIndices(
                    tokenizer.mapTokensToWords(tokensFromResult),
                    rawOutput2.joinToString().toInt(),
                    rawOutput3.joinToString().toInt()
                )
                Log.i("DEBUG_INDEX_CONVERSION", "Index Conversion Result: $textIndices")
                if (textIndices.isNotEmpty()) {
                    result.startIndex = textIndices[0]
                    result.endIndex = textIndices[textIndices.size - 1]
                } else {
                    // Handle the case when text_indices is empty
                    result.startIndex = 0
                    result.endIndex = 0
                }
                Log.i("DEBUG_FINAL_ANSWER", "Final Answer: ${result.outputAnswer}")
            }
        }
        return result
    }
}
