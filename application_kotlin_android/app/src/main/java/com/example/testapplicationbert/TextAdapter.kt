// TextAdapter.kt
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.testapplicationbert.R
import com.example.testapplicationbert.TextItem

// RecyclerView adapter for displaying a list of TextItems
class TextAdapter(private val items: List<TextItem>, private val onItemClick: (TextItem) -> Unit) :
    RecyclerView.Adapter<TextAdapter.ViewHolder>() {

    // Inflates the item view layout and returns a new ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_text, parent, false)
        return ViewHolder(view)
    }

    // Binds data to the ViewHolder at the specified position
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)

        // Set click listener to handle item click events
        holder.itemView.setOnClickListener { onItemClick(item) }
    }

    // Returns the total number of items in the data set
    override fun getItemCount(): Int {
        return items.size
    }

    // ViewHolder class for caching views
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val titleTextView: TextView = itemView.findViewById(R.id.titleTextView)

        // Binds data to the views in the ViewHolder
        fun bind(item: TextItem) {
            titleTextView.text = item.title
            // Bind other data to the view...
        }
    }
}
