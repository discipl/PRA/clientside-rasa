package com.example.testapplicationbert

// Import the TextAdapter class
import TextAdapter
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

// MainActivity.kt: Entry point of the application
class MainActivity : AppCompatActivity() {

    // Called when the activity is first created
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Set the layout for this activity
        setContentView(R.layout.activity_main)

        // Path to the YAML file containing text data
        val filePath = "res/raw/input_file.yaml"

        // Create an instance of YamlParser to parse the YAML file
        val yamlParser = YamlParser()

        // Parse the YAML file and get a list of text items
        val textItems = yamlParser.parseYamlFile(filePath)

        // Get the RecyclerView from the layout
        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)

        // Set the layout manager for the RecyclerView
        recyclerView.layoutManager = LinearLayoutManager(this)

        // Set the adapter for the RecyclerView, providing the list of text items
        recyclerView.adapter = TextAdapter(textItems) { textItem ->
            // Define what happens when a text item is clicked
            val intent = Intent(this, FullTextActivity::class.java)
            intent.putExtra("title", textItem.title)
            intent.putExtra("fullText", textItem.fullText)
            startActivity(intent)
        }

        // Add a divider between items in the RecyclerView
        val dividerItemDecoration = DividerItemDecoration(
            recyclerView.context,
            LinearLayoutManager.VERTICAL
        )
        recyclerView.addItemDecoration(dividerItemDecoration)
    }
}
