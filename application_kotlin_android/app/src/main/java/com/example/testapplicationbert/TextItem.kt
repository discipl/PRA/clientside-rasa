package com.example.testapplicationbert

data class TextItem(val title: String, val fullText: String)
