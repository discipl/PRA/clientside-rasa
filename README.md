# Clientside Rasa

This is the Repository (as the name might not suggest) for multiple projects related to the Clientside implementation of a chatbot.

In alphabetical order

- application_kotlin_android -> represent a full implementation of a Q&A Chatbot using DistilBERT en/nl converted in ONNX with pre and post processing with examples, text highlighting, and jumping to text functionality in Kotlin (and Java).
- javascript_example -> simplest example for integrating a Q&A implementation in JavaScript 
- model_finetuning -> all trained models were done using notebooks presented here. Also includes the NL SQUAD V2 training dataset after manual cleaning, a conversion of mobileBERT in PyTorch (untrained)
- tensorflow_removal -> the source code of RASA with tensorflow and scikit removed (the dependencies might appear in code but they are never loaded, the actual source code has not been cleaned of all possible references to tensorflow and scikit).
- test_tensorflow_NLU_model -> notebook for a simple attempt at recreating a RASA architecture using exclusively tensorflow
- zero_shot_learning_example -> simple notebook showing the capability of a zero-shot NLU model. It is based on the filters provided in Figma, and acts as a sort of conversational chatbot, by enabling users to type a sentence and selecting the answer based on that

## Sidenotes

Some of the larger .json or ML model files in the project use Git LFS. See .gitattributes for all files that are automatically using Git LFS and ensure updates are properly represented in the .gitattributes. 

IMPORTANT: When pulling from this repository ensure git-lfs is installed on the computer, otherwise files stored with git-lfs will not be pulled, but only a pointer to them.