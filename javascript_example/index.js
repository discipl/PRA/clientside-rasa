import { pipeline, env } from 'https://cdn.jsdelivr.net/npm/@xenova/transformers@2.6.0';

// Since we will download the model from the Hugging Face Hub, we can skip the local model check
env.allowLocalModels = false;

// Reference the elements that we will need
const status = document.getElementById('status');
const contextInput = document.getElementById('context');
const questionInput = document.getElementById('question');
const answerElement = document.getElementById('answer');

// Create a new question answering pipeline
status.textContent = 'Loading model...';
const questionAnswerer = await pipeline('question-answering', 'tclungu/distilbert-base-en-nl-cased-finetuned-squad');
status.textContent = 'Ready';

// Function to handle question answering
window.answerQuestion = function () {
    const context = contextInput.value;
    const question = questionInput.value;

    if (!context || !question) {
        alert('Please enter both context and question.');
        return;
    }

    status.textContent = 'Answering...';

    // Use the question answering pipeline
    questionAnswerer(
        question,
        context,
    ).then(output => {
        status.textContent = '';
        answerElement.textContent = `Answer: ${JSON.stringify(output, null, 2)}`;
    }).catch(error => {
        status.textContent = 'Error occurred during question answering.';
        console.error('Pipeline error:', error);
    });
};


